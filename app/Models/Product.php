<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Screen\AsSource;

class Product extends Model
{

    use HasFactory, AsSource, Attachable;

    protected $fillable = ['name', 'price', 'is_available', 'description', 'img_url'];

    public function icon()
    {
        return $this->hasOne(Attachment::class, 'id', 'img_url')->withDefault();
    }
}
