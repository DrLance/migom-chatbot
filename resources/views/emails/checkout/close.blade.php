@component('mail::message')
# You did not finish your purchase

To continue, follow the link.

@component('mail::button', ['url' => $link, 'color' => 'success'])
    Continue
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
