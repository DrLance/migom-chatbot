<?php

namespace App\Console\Commands;

use App\Http\Controllers\AMOCrmController;
use Illuminate\Console\Command;

class AmoCrmRefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amo_crm:refresh_token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $amoCrm = new AMOCrmController();
        $amoCrm->getAccessToken();

        return 0;
    }
}
