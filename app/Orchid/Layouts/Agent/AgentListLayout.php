<?php

namespace App\Orchid\Layouts\Agent;

use App\Models\AgentReferral;
use App\Models\CPaymentCallbacks;
use App\Models\User;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class AgentListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'c_payment_callbacks';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
          TD::make('email', 'Client Email'),
          TD::make('amount', 'Amount')->render(function (CPaymentCallbacks $cPayment){
              return number_format($cPayment->amount, 5);
          }),
          TD::make('real_amount', 'Paid Amount')->render(function (CPaymentCallbacks $cPayment){
              return number_format($cPayment->real_amount, 5);
          }),
          TD::make('diff_amount', 'Diff Amount')->render(function (CPaymentCallbacks $cPayment){
              return number_format($cPayment->real_amount - $cPayment->amount, 5);
          }),
          TD::make('currency', 'currency'),
          TD::make('real_price', 'Price'),
          TD::make('commission', 'Agent Сommission')->render(function (CPaymentCallbacks $lead) {

              $comm = 0;

              if($lead->confirms !== 'GREEN') return $comm;

              $user = User::where('referral_code', $lead->referall_code)->first();

              if($user) {

                  $agentReferral = AgentReferral::where('user_id', $user->id)->first();

                  if($agentReferral && $agentReferral->level == 1) {
                      $comm = $lead->real_price * 0.3;
                  } else {
                      $comm = $lead->real_price * 0.05;
                  }

              }

              return number_format($comm, 5);
          }),
          TD::make('confirms', 'Confirms')->render(function (CPaymentCallbacks $lead){
              if($lead->confirms) {
                  return '<span>' . $lead->confirms . '</span>';
              } else {
                  return '<span>PENDING</span>';
              }
          }),
          TD::make('status', 'Status')->render(function (CPaymentCallbacks $lead){
              if($lead->status == 100) {
                  return '<span>PAID</span>';
              } else {
                  return '<span>NOT PAID</span>';
              }
          }),
          TD::make('status_text', 'Status Text'),
        ];
    }
}
