<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Alert;

class ProductEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ProductEditScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'ProductEditScreen';

    public $exists = false;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Product $product): array
    {
        $this->exists = $product->exists;

        if ($this->exists) {
            $this->name = 'Edit Product';
        }

        $product->load('attachment');

        return [
          'product' => $product,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
          Button::make('Create Product')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(! $this->exists),

          Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

          Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
          Layout::rows([
            Input::make('product.name')
                 ->title('Name')->required(),

            TextArea::make('product.description')
                    ->title('Description')
                    ->rows(3)
                    ->maxlength(200),

            Input::make('product.price')
                 ->title('Price')
                 ->required(),

            CheckBox::make('product.is_available')
              ->title('Is Available')
              ->value(1)
                    ->sendTrueOrFalse(),

            Picture::make('product.img_url')
                   ->targetId(),
          ]),
        ];
    }

    /**
     * @param Product $product
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Product $product, Request $request)
    {
        $product->fill($request->get('product'))->save();

        $product->attachment()->sync(
          $request->input('product.attachment', [])
        );

        Alert::info('You have successfully created an Product.');

        return redirect()->route('platform.product.list');
    }

    public function remove(Product $product)
    {
        $product->delete();

        Alert::info('You have successfully deleted the Product.');

        return redirect()->route('platform.product.list');
    }
}
