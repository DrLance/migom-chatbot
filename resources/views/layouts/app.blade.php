<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="robots" content="noindex, nofollow"/>

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap">

  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">

  @routes
  <script src="{{ mix('js/app.js') }}" defer></script>

</head>
<body>

<main class="main" id="emmi-app">

  <header class="container header">
    <a href="{{route('home')}}">
      <img src="/img/logo.svg" alt="logo"/>
    </a>

    <?php
    $isHide = true;

    if(Session::has('data')) {
        $isHide = false;
    }
    ?>

    <cart-icon-component :ishide="{{json_encode($isHide)}}"></cart-icon-component>

  </header>
  @yield('content')
</main>

<footer class="footer">
  <div class="footer-columns__container container">
    <div class="footer-column">
      <p class="footer-column__header">Documents</p>
      <a href="https://migom.com/licenses" class="footer-column__link" target="_blank">Migom Licenses</a>
      <a href="https://drive.google.com/open?id=1btDymbPKKIcmKQ9tybKBmKuJUv3FyKKy" class="footer-column__link" target="_blank">Public documents</a>
    </div>
    <div class="footer-column">
      <p class="footer-column__header">Products</p>
      <a href="https://migom.com" class="footer-column__link" target="_blank">Migom Bank</a>
      <a href="https://medium.com/@migombank/how-to-buy-crypto-from-your-bank-account-and-not-get-blocked-migom-bank-approach-55e572e6f2a1" class="footer-column__link" target="_blank">Crypto conversion</a>
      <a href="https://migom.com/investor-relations" class="footer-column__link" target="_blank">Investor relations</a>
    </div>
    <div class="footer-column">
      <p class="footer-column__header">Affiliated companies</p>
      <a href="https://migom.ch/" class="footer-column__link" target="_blank">Migom Verwaltung</a>
    </div>
    <div class="footer-column">
      <p class="footer-column__header">Community</p>
      <a href="https://t.me/migombank" class="footer-column__link" target="_blank">
        <img src="/img/tg.svg" alt="Telegram chatbot"/>Telegram chatbot
      </a>
      <a href="https://twitter.com/MigomBank" class="footer-column__link" target="_blank">
        <img src="/img/tw.svg" alt="Twitter"/>Twitter
      </a>
      <a href="https://www.linkedin.com/company/migom/" class="footer-column__link" target="_blank">
        <img src="/img/ln.svg" alt="LinkedIn"/>LinkedIn
      </a>
      <a href="https://www.facebook.com/migombank" class="footer-column__link" target="_blank">
        <img src="/img/fb.svg" alt="LinkedIn"/>Facebook
      </a>
      <a href="https://medium.com/@migombank/introducing-migom-a-global-bank-for-the-emerging-markets-bed9f9778894" class="footer-column__link" target="_blank">
        <img src="/img/mb.svg" alt="Migom Blog"/>Migom Blog
      </a>
    </div>
  </div>
  <div class="footer-copyright container">
    <span class="footer-copyright__header">© 2019-2020 Migom Bank Ltd.</span>
    <span class="footer-copyright__description">Migom Bank Ltd. is an International Bank regulated by the Financial Services Unit of the Ministry of Finance of the Commonwealth of Dominica, License number: 08082019.
SWIFT BIC: MIMODMD2 FATCA GIIN: 58NW3B.99999.SL.212
Our address is: 75 Hillsborough Street, 2nd Floor, Roseau, Commonwealth of Dominica | Telephone: +1 767 448 4297 | Email: info@migom.com
Payment and e-money services offered via Migom Investments S.A., Luxembourg, an EMD Agent registered by the UK FCA, Reference Number 902737 | Telephone +41 41 552 4030 support@migom.ch
Private banking, wealth managemet and prepaid card services offered via Migom Verwaltung AG, Switzerland, Member of SRO ARIF, recognized by FINMA
    </span>
  </div>
</footer>

@stack('footer_js')
</body>
</html>
