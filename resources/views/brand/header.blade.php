@push('head')
  <link
    href="/favicon.ico"
    id="favicon"
    rel="icon"
  >
@endpush

<p class="h2 n-m font-thin v-center">
  <img height="48" width="48" src="{{asset('img/logo-small.png')}}"  alt="Migom" />

  <span class="m-l d-none d-sm-block">
        Migom
        <small class="v-top opacity">Chatbot</small>
    </span>
</p>
