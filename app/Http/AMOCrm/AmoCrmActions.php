<?php


namespace App\Http\AMOCrm;


use App\Http\Controllers\AMOCrmController;
use App\Models\AmoCrm;
use Carbon\Carbon;

class AmoCrmActions
{

    public function addLead($name, $price, $items = [])
    {
        $amo = new AMOCrmController();

        $link           = config('amo.url') . 'private/api/v2/json/pipelines/list';
        $amoCrm         = AmoCrm::first();
        $pipelineId     = 3676741;
        $pipelineStatus = null;

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');

        $response = json_decode($response);

        foreach ($response->response->pipelines as $pipeline) {

            if ($pipeline->name == config('amo.funnel')) {
                $pipelineId     = $pipeline->id;
                $pipelineStatus = $pipeline->statuses;
            }

        }


        $pipelineStatus = collect($pipelineStatus);

        $pipelineStatusId = $pipelineStatus->where('name', config('amo.no_payment'))->pluck('id');

        $link   = config('amo.url') . 'private/api/v2/json/leads/set';
        $amoCrm = AmoCrm::first();

        $leads['request']['leads']['add'] = [
          [
            'name'          => $name,
            'email'         => $name,
            'price'         => $price,
            'pipeline_id'   => $pipelineId,
            'status_id'     => $pipelineStatusId->first(),
            'custom_fields' => [
              [
                'id'     => 160433,
                'values' => $items,
              ],
            ],
          ],
        ];

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, $leads, 'POST');
        $response = json_decode($response);

        $leadId = $response->response->leads->add[0]->id;

        return $leadId;
    }

    public function updateLead($leadId, $statusText = 'paid')
    {
        $amo = new AMOCrmController();

        $link           = config('amo.url') . 'private/api/v2/json/pipelines/list';
        $amoCrm         = AmoCrm::first();
        $pipelineStatus = null;

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');

        $response = json_decode($response);

        foreach ($response->response->pipelines as $pipeline) {

            if ($pipeline->name == config('amo.funnel')) {
                $pipelineStatus = $pipeline->statuses;
            }
        }


        $pipelineStatus = collect($pipelineStatus);

        $pipelineStatus = $pipelineStatus->map(function ($item) {
            $item->name = mb_strtolower($item->name);

            return $item;
        });

        $pipelineStatusId = $pipelineStatus->where('name', $statusText)->pluck('id');


        $link   = config('amo.url') . 'private/api/v2/json/leads/set';
        $amoCrm = AmoCrm::first();

        $leads['request']['leads']['update'] = [
          [
            'id'            => $leadId,
            'status_id'     => $pipelineStatusId->first(),
            'last_modified' => Carbon::now()->timestamp,
          ],
        ];

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, $leads, 'POST');

        return $response;
    }

    public function getCustomFields()
    {
        $amo = new AMOCrmController();

        $link   = config('amo.url') . 'private/api/v2/json/accounts/current';
        $amoCrm = AmoCrm::first();

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');

        $response = json_decode($response);

        return $response;
    }

}
