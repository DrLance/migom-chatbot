<?php

declare(strict_types=1);

namespace App\Orchid\Screens;

use App\Models\AgentReferral;
use App\Models\CPaymentCallbacks;
use App\Models\User;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class PlatformScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Dashboard';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];

 /*       return [
            Link::make('Website')
                ->href('http://orchid.software')
                ->icon('globe-alt'),

            Link::make('Documentation')
                ->href('https://orchid.software/en/docs')
                ->icon('docs'),

            Link::make('GitHub')
                ->href('https://github.com/orchidsoftware/platform')
                ->icon('social-github'),
        ];*/
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        $user = auth()->user();
        $coef = 0.3;
        $childLeadsSum = 0;

        if($user->referralLevel) {
            $level = $user->referralLevel->level;

            if($level == 2) {
                $coef = 0.05;
            }

            $childReferral = AgentReferral::where('referral_user_id', $user->id)->get();

            $childUsers = User::whereIn('id',$childReferral->pluck('user_id')->toArray())->get();

            $childLeads = CPaymentCallbacks::whereIn('referral_code', $childUsers->pluck('referral_code')->toArray())
                                           ->where('confirms', 'GREEN')
                                           ->get();

            $childLeadsSum = $childLeads->sum('real_price') * 0.05;

        }

        $leads = CPaymentCallbacks::where('referral_code', $user->referral_code)->where('confirms', 'GREEN')->get();

        $leadsSum = ($leads->sum('real_price') * $coef) + $childLeadsSum;

        return [
            Layout::view('welcome', ['leads' => $leads, 'leads_sum' => $leadsSum]),
        ];
    }
}
