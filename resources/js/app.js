import Vue from 'vue';
import Vuex from 'vuex';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';


import CartComponent from "./components/CartComponent";
import CheckoutComponent from "./components/CheckoutComponent";
import CheckoutFinishComponent from "./components/CheckoutFinishComponent";
import CartIconComponent from "./components/CartIconComponent";
import PincodeInput from 'vue-pincode-input';
import TooltipComponent from "./components/TooltipComponent";

require('./bootstrap');

const inertiApp = document.getElementById('inertia-app');
const app = document.getElementById('emmi-app');


function validateEmail (email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

Vue.mixin({
  methods: {
    route,
  }
});

Vue.component('cart-component', CartComponent);
Vue.component('checkout-component', CheckoutComponent);
Vue.component('checkout-finish-component', CheckoutFinishComponent);
Vue.component('cart-icon-component', CartIconComponent);
Vue.component('PincodeInput', PincodeInput);
Vue.component('tooltip-component', TooltipComponent);

Vue.use(Vuex);
Vue.prototype.$vueEventBus = new Vue();

const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
});

Vue.directive('click-outside', {
  bind: function(el, binding, vnode) {
    el.clickOutsideEvent = function(event) {
      // here I check that click was outside the el and his children
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function(el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});

if (inertiApp) {
  Vue.use(InertiaApp);
  Vue.use(InertiaForm);
  Vue.use(PortalVue);

  new Vue({
    render: (h) =>
      h(InertiaApp, {
        props: {
          initialPage: JSON.parse(app.dataset.page),
          resolveComponent: (name) => require(`./Pages/${name}`).default,
        },
      }),
  }).$mount(inertiApp);
}

if (app) {
  new Vue({
    store
  }).$mount(app);
}



window.validateEmail = validateEmail;

