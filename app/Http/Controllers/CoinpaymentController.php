<?php


namespace App\Http\Controllers;


use App\Http\AMOCrm\AmoCrmActions;
use App\Http\Api\ApiSumSub;
use App\Mail\SuccessPaymentLessNotification;
use App\Mail\SuccessPaymentNotification;
use App\Models\CPaymentCallbacks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CoinpaymentController
{
    public function updateStatus(Request $request)
    {
        $data = $request->all();

        $cPaymentCallback = CPaymentCallbacks::where('address', $data['address'] ?? '')->first();

        Log::debug('START UPDATE STATUS');
        Log::debug(json_encode($request->all()));

        if ($cPaymentCallback) {
            $cPaymentCallback->status      = $data['status'];
            $cPaymentCallback->status_text = $data['status_text'];
            $cPaymentCallback->txn_id      = $data['txn_id'];
            $cPaymentCallback->real_amount = $data['amount'];

            $cPaymentCallback->save();

            $link = route('bank.checkout.finish', ['code' => $cPaymentCallback->address]);

            if ($cPaymentCallback->real_amount >= $cPaymentCallback->amount) {
                $amoActions = new AmoCrmActions();

                $response = $amoActions->updateLead($cPaymentCallback->lead_id);

                Mail::to($cPaymentCallback->email)->send(new SuccessPaymentNotification($link));

                Log::debug($response);
            } else {
                Mail::to($cPaymentCallback->email)->send(new SuccessPaymentLessNotification($link));

                Log::debug('Payment for a lesser amount');
            }


        } else {
            Log::debug('ADDRESS NOT FOUND');
        }

    }

    public function checkStatus(Request $request)
    {
        $testObject = new ApiSumSub();

        $response['error']   = false;
        $response['message'] = false;


        $address = $request->input('address', null);
        $email   = $request->input('email');

        $cPaymentCallback = CPaymentCallbacks::where(function ($q) use ($email, $address) {
            $q->where('address', $address)
              ->orWhere('email', $email);
        })->orderBy("updated_at", "DESC")->first();


        if ($cPaymentCallback) {

            if ($cPaymentCallback->status == 100) {
                $response['message'] = true;
                $applicantId         = $cPaymentCallback->sumsub_applicant_id;

                if ( ! $applicantId) {
                    $applicantId                           = $testObject->createApplicant($cPaymentCallback->id);
                    $cPaymentCallback->sumsub_applicant_id = $applicantId;
                    $cPaymentCallback->save();
                }

                $accessTokenStr = $testObject->getAccessToken($cPaymentCallback->id);

                $response['token'] = json_decode($accessTokenStr->getContents());

            }

        }


        return response()->json($response);
    }

    public function updateCheckStatus(Request $request)
    {

        $response['error']   = false;
        $response['message'] = false;

        $address = $request->input('address', null);

        if ($address) {
            $cPaymentCallback = CPaymentCallbacks::where('address', $address)->first();

            $cPaymentCallback->sumsub_applicant_id = $request->input('applicantId', null);

            $cPaymentCallback->save();

        }

        return response()->json($response);
    }

    public function sumSubHook(Request $request)
    {
        $amoActions = new AmoCrmActions();

        try {

            $data = $request->all();

            $reviewAnswer = 'RED';

            if (isset($data['applicantId'])) {

                $applicantId = $data['applicantId'];

                if (isset($data['reviewResult'])) {
                    $reviewAnswer = $data['reviewResult']['reviewAnswer'];
                }
                $cPaymentCallback = CPaymentCallbacks::where('sumsub_applicant_id', $applicantId)->first();

                if ($reviewAnswer === 'GREEN') {

                    if ($cPaymentCallback) {
                        $response = $amoActions->updateLead($cPaymentCallback->lead_id, 'kyc');
                        Log::debug($response);
                    }
                }


                $cPaymentCallback->confirms = $reviewAnswer;
                $cPaymentCallback->save();

                Log::debug($applicantId);
            }
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
        }

    }

    public static function coinpayments_api_call($cmd, $req = array())
    {
        // Fill these in from your API Keys page
        $public_key  = config('coins.coinpayment_public');
        $private_key = config('coins.coinpayment_private');

        // Set the API command and required fields
        $req['version'] = 1;
        $req['cmd']     = $cmd;
        $req['key']     = $public_key;
        $req['format']  = 'json'; //supported values are json and xml

        // Generate the query string
        $post_data = http_build_query($req, '', '&');

        // Calculate the HMAC signature on the POST data
        $hmac = hash_hmac('sha512', $post_data, $private_key);

        // Create cURL handle and initialize (if needed)
        static $ch = null;
        if ($ch === null) {
            $ch = curl_init(config('coins.coinpayment_url'));
            curl_setopt($ch, CURLOPT_FAILONERROR, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: ' . $hmac));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        // Execute the call and close cURL handle
        $data = curl_exec($ch);
        // Parse and return data if successful.
        if ($data !== false) {
            if (PHP_INT_SIZE < 8 && version_compare(PHP_VERSION, '5.4.0') >= 0) {
                // We are on 32-bit PHP, so use the bigint as string option. If you are using any API calls with Satoshis it is highly NOT recommended to use 32-bit PHP
                $dec = json_decode($data, true, 512, JSON_BIGINT_AS_STRING);
            } else {
                $dec = json_decode($data, true);
            }
            if ($dec !== null && count($dec)) {
                return $dec;
            } else {
                // If you are using PHP 5.5.0 or higher you can use json_last_error_msg() for a better error message
                return array('error' => 'Unable to parse JSON result (' . json_last_error() . ')');
            }
        } else {
            return array('error' => 'cURL error: ' . curl_error($ch));
        }
    }

}
