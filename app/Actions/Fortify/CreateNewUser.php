<?php

namespace App\Actions\Fortify;

use App\Mail\SendUserPasswordRegistration;
use App\Models\AgentReferral;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Orchid\Platform\Models\Role;
use Orchid\Support\Facades\Alert;


class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     *
     * @return \App\Models\User
     */
    public function create(array $input)
    {

        Validator::make($input, [
          'name'              => ['required', 'string', 'max:255'],
          'email'             => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'solicitation_form' => ['required'],
          'terms'             => ['required'],
        ])->validate();

        $password = Str::random('8');

        Mail::to($input['email'])->send(new SendUserPasswordRegistration($password));

        $ref = Session::get('ref',null);

        $user = User::create([
          'name'          => $input['name'],
          'email'         => $input['email'],
          'password'      => Hash::make($password),
          'referral_code' => uniqid('ref_'),
        ]);

        $role = Role::where('slug', 'agent')->first();

        if ( ! $role) {
            $role       = new Role();
            $role->slug = 'agent';
            $role->name = 'agent';
            $role->save();
        }

        $user->addRole($role);


        if($ref) {
            $agentReferral = new AgentReferral();
            $agentReferral->user_id = $user->id;
            $agentReferral->level = 2;
            $userReferral = User::where('referral_code', $ref)->first();

            $agentReferral->referral_user_id = $userReferral->id;

            $agentReferral->save();
        } else {
            $agentReferral = new AgentReferral();
            $agentReferral->user_id = $user->id;
            $agentReferral->level = 1;

            $agentReferral->save();
        }

        Alert::success('Congratulations on your successful registration!');

        return $user;
    }
}
