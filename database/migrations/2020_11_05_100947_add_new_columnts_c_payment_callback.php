<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumntsCPaymentCallback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('c_payment_callbacks', function (Blueprint $table) {
            $table->string('referral_code')->nullable();
            $table->decimal('real_price', 10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('c_payment_callbacks', function (Blueprint $table) {
            $table->dropColumn('referral_code');
            $table->dropColumn('real_price');
        });
    }
}
