<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('update-status',[\App\Http\Controllers\CoinpaymentController::class, 'updateStatus']);

Route::post('update-check-status',[\App\Http\Controllers\CoinpaymentController::class, 'updateCheckStatus'])->name('api.cp.update_check_status');

Route::post('sumsub-webhook',[\App\Http\Controllers\CoinpaymentController::class, 'sumSubHook'])->name('api.cp.sum_sub_hook');

Route::get('deronica',[\App\Http\AMOCrm\DeronicaCrmActions::class, 'addLead']);
Route::post('deronica',[\App\Http\AMOCrm\DeronicaCrmActions::class, 'addLead']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
