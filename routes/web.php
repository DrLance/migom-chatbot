<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('get-account',[\App\Http\Controllers\HomeController::class, 'getAccount'])->name('get.account');
Route::get('bank-account',[\App\Http\Controllers\HomeController::class, 'bankAccount'])->name('bank.account');
Route::get('checkout',[\App\Http\Controllers\HomeController::class, 'checkoutView'])->name('bank.checkout');
Route::get('checkout-finish',[\App\Http\Controllers\HomeController::class, 'checkoutFinishView'])->name('bank.checkout.finish');
Route::get('test',[\App\Http\Controllers\HomeController::class, 'test']);
Route::get('update-token',[\App\Http\Controllers\AMOCrmController::class, 'getRefreshToken']);
Route::get('refresh-token',[\App\Http\Controllers\AMOCrmController::class, 'getAccessToken']);

Route::post('check-status',[\App\Http\Controllers\CoinpaymentController::class, 'checkStatus'])->name('api.cp.check_status');
Route::post('checkout', [\App\Http\Controllers\HomeController::class, 'proceedCheckout'])->name('api.checkout');
Route::post('checkout-finish', [\App\Http\Controllers\HomeController::class, 'checkoutFinish'])->name('api.checkout.finish');
Route::post('checkout-finish-close', [\App\Http\Controllers\HomeController::class, 'checkoutFinishClose'])->name('api.checkout.close');
Route::post('/mail/send-code-notification', [\App\Http\Api\MailNotifications::class, 'sendEmailCode'])->name('api.mail.send_code');
Route::post('/mail/check-code-notification', [\App\Http\Api\MailNotifications::class, 'checkEmailCode'])->name('api.mail.check_code');
Route::post('/log-activity/store', [\App\Http\Controllers\LogActivityController::class, 'store'])->name('log_activity.store');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');
