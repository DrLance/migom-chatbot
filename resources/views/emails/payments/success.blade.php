@component('mail::message')
# Your payment has been received

To continue, follow the link.

@component('mail::button', ['url' => $link, 'color' => 'success'])
  Continue
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
