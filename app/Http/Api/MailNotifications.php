<?php


namespace App\Http\Api;
use App\Http\Controllers\Controller;
use App\Mail\SendCodeVerification;
use App\Models\EmailCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class MailNotifications extends Controller
{

    public function sendEmailCode(Request  $request) {
        $email  = $request->input('email', null);

        $response['error']   = false;
        $response['message'] = 'OK';

        if($email) {
            $code = rand(1000, 9999);

            Mail::to($email)->send(new SendCodeVerification($code));

            $emailCode = EmailCode::where('email', $email)->first();

            if(!$emailCode) {
                $emailCode = new EmailCode();
                $emailCode->email = $email;
            }

            $emailCode->code = $code;

            $emailCode->save();
        }

        return response()->json($response);

    }

    public function checkEmailCode(Request  $request) {
        try {
            $email = $request->input('email', null);
            $code  = $request->input('code', null);

            $response['error']                = false;
            $response['message']              = 'OK';
            $response['data']['verification'] = false;

            if ($email && $code) {

                $emailCode = EmailCode::where('email',$email)->where('code', $code)->first();

                if ($emailCode) {
                    $emailCode->verification_at       = Carbon::now();
                    $response['data']['verification'] = true;
                    $emailCode->save();

                    $data = $request->session()->get('data', null);

                    if($data) {
                        $data['email'] = $email;
                        $request->session()->put('data',$data);
                    }
                }

            }
        } catch (\Exception $e) {
            $response['error']                = true;
            $response['message']              = $e->getMessage();
        }

        return response()->json($response);

    }

}
