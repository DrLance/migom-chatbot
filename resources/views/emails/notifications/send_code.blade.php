@component('mail::message')

  Enter the code to validate your mail

  <p style="font-size: 24px; font-weight: bold">{{$code}}</p>


  Thanks,<br>
  {{ config('app.name') }}
@endcomponent
<?php
