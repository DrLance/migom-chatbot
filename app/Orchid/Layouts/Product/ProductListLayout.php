<?php

namespace App\Orchid\Layouts\Product;

use App\Models\Product;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;

class ProductListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'products';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
          TD::make('img_url', 'Icon')
            ->width(100)
            ->render(function (Product $product) {

                $img = $product->icon()->first();

                return "<img src='{$img->url()}' alt='sample' class='mw-100 d-block img-fluid'>";
            }),

          TD::make('name', 'Name')
            ->render(function (Product $product) {
                return Link::make($product->name)
                           ->route('platform.product.edit', $product);
            }),

          TD::make('description', 'description'),
          TD::make('price', 'Price'),
          TD::make('is_available', 'Is Available'),

        ];
    }
}
