<?php

namespace App\Orchid\Screens\Agent;

use App\Models\CPaymentCallbacks;
use App\Orchid\Layouts\Agent\AgentListLayout;
use Orchid\Screen\Screen;

class AgentListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Agent Referrals';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'Current leads and commissions';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $user = auth()->user();
        $leads = collect();

        if($user->hasAccess('platform.systems.users')) {
            $leads = CPaymentCallbacks::paginate();
        } else {
            $leads = CPaymentCallbacks::where('referral_code', $user->referral_code)->where('confirms', 'GREEN')->paginate();
        }

        return [
          'c_payment_callbacks' => $leads,
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
          AgentListLayout::class,
        ];
    }
}
