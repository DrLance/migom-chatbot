@extends('layouts.app')

@section('content')
  <section class="container">
    <checkout-finish-component :items="{{json_encode($items)}}"></checkout-finish-component>
  </section>
@endsection

