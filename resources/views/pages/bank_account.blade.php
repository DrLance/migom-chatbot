@extends('layouts.app')

@section('content')
  <section class="container">
    <cart-component :data="{{json_encode($data)}}" :items="{{json_encode($items)}}"></cart-component>
  </section>

@endsection

