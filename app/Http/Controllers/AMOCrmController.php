<?php


namespace App\Http\Controllers;

use App\Models\AmoCrm;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;


class AMOCrmController
{


    public function getRefreshToken(Request $request)
    {
        $data = [
          'client_id'     => config('amo.integration'),
          'client_secret' => config('amo.secret'),
          'grant_type'    => 'authorization_code',
          'code'          => $request->input('code'),
          'redirect_uri'  => 'https://migomgroup.amocrm.ru/',
        ];

        $response = $this->getRequest($data);

        $amoCrm = AmoCrm::first();

        if(!$amoCrm) {
            $amoCrm = new AmoCrm();
        }

        $amoCrm->refresh_token = $response['refresh_token'];
        $amoCrm->access_token = $response['access_token'];

        $amoCrm->save();

        dump($response);

    }


    public function getAccessToken()
    {

        $amoCrm = AmoCrm::first();


        $data = [
          'client_id'     => config('amo.integration'),
          'client_secret' => config('amo.secret'),
          'grant_type'    => 'refresh_token',
          'refresh_token' => $amoCrm->refresh_token,
          'redirect_uri'  =>  config('amo.url') ,
        ];

        $response = $this->getRequest($data);

        $amoCrm->refresh_token = $response['refresh_token'];
        $amoCrm->access_token = $response['access_token'];

        $amoCrm->save();

        dump($response);

        Log::debug($response);

    }

    public function getRequest($data)
    {

        $link = config('amo.url') . 'oauth2/access_token'; //Формируем URL для запроса
        /**
         * Нам необходимо инициировать запрос к серверу.
         * Воспользуемся библиотекой cURL (поставляется в составе PHP).
         * Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
         */
        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        /** Устанавливаем необходимые опции для сеанса cURL  */
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        $out  = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        /** Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code   = (int)$code;
        $errors = [
          400 => 'Bad request',
          401 => 'Unauthorized',
          403 => 'Forbidden',
          404 => 'Not found',
          500 => 'Internal server error',
          502 => 'Bad gateway',
          503 => 'Service unavailable',
        ];

        try {
            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
            }
        } catch (\Exception $e) {
            die('Ошибка: ' . $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode());
        }


        $response = json_decode($out, true);

        return $response;
    }

    public function getRequestBearer($access_token, $link , $data = [], $type = 'GET')
    {

         //Формируем URL для запроса
        /** Получаем access_token из вашего хранилища */

        /** Формируем заголовки */
        $headers = [
          'Authorization: Bearer ' . $access_token,
        ];
        /**
         * Нам необходимо инициировать запрос к серверу.
         * Воспользуемся библиотекой cURL (поставляется в составе PHP).
         * Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
         */
        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        /** Устанавливаем необходимые опции для сеанса cURL  */
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-oAuth-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);

        if($type = 'POST') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $out  = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        /** Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
        $code   = (int)$code;
        $errors = [
          400 => 'Bad request',
          401 => 'Unauthorized',
          403 => 'Forbidden',
          404 => 'Not found',
          500 => 'Internal server error',
          502 => 'Bad gateway',
          503 => 'Service unavailable',
        ];

        try {
            /** Если код ответа не успешный - возвращаем сообщение об ошибке  */
            if ($code < 200 || $code > 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
            }
        } catch (\Exception $e) {
            die('Ошибка: ' . $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode());
        }

        return $out;
    }

}
