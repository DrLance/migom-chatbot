<?php

namespace App\Orchid\Screens\Product;

use App\Models\Product;
use App\Orchid\Layouts\Product\ProductListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class ProductListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'ProductListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'ProductListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
          'products' => Product::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
          Link::make('Create new')
              ->icon('pencil')
              ->route('platform.product.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
          ProductListLayout::class,
        ];
    }
}
