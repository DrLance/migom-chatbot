@component('mail::message')

  Your password: <p>{{$password}}</p>

@component('mail::button', ['url' => route('platform.main')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
