<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCPaymentCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_payment_callbacks', function (Blueprint $table) {
            $table->id();
            $table->text('address');
            $table->string('currency');
            $table->string('email');
            $table->string('ipn_url')->nullable();
            $table->double('amount',10,10);
            $table->bigInteger('lead_id')->nullable();
            $table->string('confirms')->nullable();
            $table->string('status')->nullable();
            $table->string('status_text')->nullable();
            $table->string('txn_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_payment_callbacks');
    }
}
