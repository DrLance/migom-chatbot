@extends('layouts.app')

@section('content')
  <section class="registration container">

    <div class="left">
      <div class="left-text__container">
        <h1>Hei it’s Micha,<br>Migom Bank’s<br>smart chat-bot</h1>
        <p class="tooltip-main">Here is a tool that will help you to get<br>acquainted with the crypto conversion<br>fee structure. </p>
      </div>
      <img class="left-man" src="/img/left-man.svg" alt="men"/>
    </div>

    <div class="form-container">
      <div class="form-container__inputs">
        <h3>Please sign in, so I could provide you with the best user experience</h3>
        @if ($errors->any())
          <div class="alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li><span class="alert-error__round">!</span>{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <form method="POST" action="{{ route('register') }}" class="form">
          @csrf
          <div class="form-input">
            <label>What’s your email adress <tooltip-component :myclass="'bank-account-card__tooltip_cart'"></tooltip-component>
              <input name="email" type="email" placeholder="example@ex.com" class="input" value="{{old('email') }}"/>
            </label>
          </div>

          <div class="form-input">
            <label>What's your full name? <tooltip-component :myclass="'bank-account-card__tooltip_cart'"></tooltip-component>
              <input name="name" type="text" placeholder="Name Surname" class="input" value="{{old('name')}}"/>
            </label>
          </div>

          <div class="form-input__checkbox">
            <div class="checkbox-container">
              <input type="checkbox" class="custom-checkbox" name="solicitation_form"/><span>I agree with <a href="/docs/non-solicitation_declaration.pdf" target="_blank">non solicitation form</a></span>
            </div>
            <div class="checkbox-container">
              <input type="checkbox" class="custom-checkbox" name="terms"/><span>I agree with <a href="#">terms & conditions</a></span>
            </div>
          </div>

          <div class="form-button__container">
            <button type="submit" class="button">Enter</button>
          </div>
        </form>
      </div>
    </div>
  </section>
@endsection
