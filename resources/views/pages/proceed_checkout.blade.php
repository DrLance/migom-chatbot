@extends('layouts.app')

@section('content')
  <section class="container">
    <checkout-component :items="{{json_encode($items)}}"></checkout-component>
  </section>
@endsection

