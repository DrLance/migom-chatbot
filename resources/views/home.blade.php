@extends('layouts.app')

@section('content')
  <section class="home-page container">

    <div class="left">

      <div class="left-text__container">
        <h1>Hei it’s Micha,<br>Migom Bank’s<br>smart chat-bot</h1>
        <p class="tooltip-main">Here is a tool that will help you to get<br>acquainted with the crypto conversion<br>fee structure. </p>
      </div>
      <img class="left-man" src="/img/left-man.svg" alt="men"/>

    </div>

    <div class="links-main-container">
      <div>
        <div class="links__container">
          <div>
            <a class="menu-item" href="{{route('bank.account')}}">
              <i class="menu-icon">🤑</i>
              Personal Bank account
              <div class="menu-arrow"></div>
            </a>
            <a class="menu-item" href="{{route('register')}}">
              <i class="menu-icon">💼</i>
              Become an agent
              <div class="menu-arrow"></div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">😎</i>
              Business Bank account
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">❓</i>
              What is Migom Bank
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">⚡</i>
              Why us
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">⚖️</i>
              Licenses and accepted jurisdictions
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">💡</i>
              FAQ
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">👩</i>
              Ask manager
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
            <a class="menu-item">
              <i class="menu-icon">🔄</i>
              Exchange BTC/USD and backwards
              <div class="menu-coming-soon">Coming Soon</div>
            </a>
          </div>

          <button type="submit" class="button button-home" onclick='window.location="{{route('get.account')}}"'>Get your account in 10 minutes</button>
        </div>
      </div>
  </section>

@endsection
