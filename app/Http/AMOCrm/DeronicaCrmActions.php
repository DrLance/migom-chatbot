<?php


namespace App\Http\AMOCrm;

use App\Http\Controllers\AMOCrmController;
use App\Models\AmoCrm;
use Illuminate\Http\Request;

class DeronicaCrmActions
{
    public function addLead(Request $request)
    {
        $apiResp = [
          'title' => 'Added',
          'msg' => 'OK',
        ];

        try {
            $you                    = $request->input('you');
            $companyName            = $request->input('company-name');
            $address                = $request->input('address');
            $countryOfIncorporation = $request->input('country-of-incorporation');
            $name                   = $request->input('name');
            $surname                = $request->input('surname');

            $title       = $request->input('title');
            $citizenship = $request->input('citizenship');
            $email       = $request->input('email');

            $amo = new AMOCrmController();

            $link   = config('amo.url') . 'private/api/v2/json/pipelines/list';
            $amoCrm = AmoCrm::first();

            $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');

            $response = json_decode($response);

            $pipelineId     = 4100986;
            $pipelineStatus = null;

            foreach ($response->response->pipelines as $pipeline) {
                if (strtolower($pipeline->name) === 'agents') {
                    $pipelineId     = $pipeline->id;
                    $pipelineStatus = $pipeline->statuses;
                }
            }

            $statusId = 38764099;

            if ( ! $pipelineId) {
                return "Error";
            }

            // $link   = config('amo.url') . 'private/api/v2/json/accounts/current';
            // $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');
            // $response = json_decode($response);

            $link = config('amo.url') . 'private/api/v2/json/leads/set';

            $leads['request']['leads']['add'] = [
              [
                'name'          => $name . ' ' . $surname . ' ' . $email,
                'email'         => $email,
                'pipeline_id'   => $pipelineId,
                'status_id'     => $statusId,
                'custom_fields' => [
                  [
                    'id'     => 601435,
                    'values' => ['value' => $address],
                  ],
                  [
                    'id'     => 601437,
                    'values' => ['value' => $citizenship],
                  ],
                  [
                    'id'     => 601453,
                    'values' => ['value' => $countryOfIncorporation],
                  ],
                  [
                    'id'     => 601455,
                    'values' => ['value' => $title],
                  ],
                  [
                    'id'     => 601457,
                    'values' => ['value' => $you . ' ' . $companyName],
                  ],
                ],
              ],
            ];

            $response = $amo->getRequestBearer($amoCrm->access_token, $link, $leads, 'POST');
            $response = json_decode($response);

            $leadId = $response->response->leads->add[0]->id;

            return response()->json($apiResp);

        } catch (\Exception $e) {
            $apiResp['title'] = 'Error';
            $apiResp['msg'] = 'Fail';

            return response()->json($apiResp);

        }

    }

}
