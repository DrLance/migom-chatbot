@include('platform::partials.alert')
<section class="d-flex flex-column align-items-start layout">
  @if(auth()->user()->referral_code)
    <h2>Your referral link: <a href="{{route('home', ['ref' => auth()->user()->referral_code])}}">{{auth()->user()->referral_code}}</a></h2>
  @endif
  <div class="">
    <div class="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
      <div class="mt-2">
        @if($leads_sum)
          <h3 class="mb-0">You earned</h3>
          <p class="text mb-1">{{$leads_sum}}</p>
        @endif
      </div>
    </div>
  </div>
</section>
