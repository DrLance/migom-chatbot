<?php

namespace App\Http\Controllers;

use App\Models\LogActivity;
use Illuminate\Http\Request;


class LogActivityController extends Controller
{
    public function store(Request  $request) {
        $data = $request->all();

        $logActivity = LogActivity::where('session_id', $request->session()->getId())->first();

        if(!$logActivity) {
            $logActivity = new LogActivity();
            $logActivity->session_id = $request->session()->getId();
        }

        $logActivity->data = json_encode($data);
        $logActivity->last_step = $request->input('last_step');

        $logActivity->save();
    }
}
