@extends('layouts.app')

@section('content')
  <section class="home-page container">

    <div class="left">
      <div class="left-text__container">
        <h1>Please choose the type of account you're willing to open</h1>
      </div>
      <img class="left-man" src="/img/left-man.svg" alt="men"/>
    </div>

    <div class="links-main-container">
      <div class="links__container">
        <div>
          <a class="menu-item" href="{{route('bank.account')}}">
            <i class="menu-icon">🤑</i>
            Personal Bank account
            <div class="menu-arrow"></div>
          </a>
          <a class="menu-item">
            <i class="menu-icon">😎</i>
            Business Bank account
            <div class="menu-coming-soon">Coming Soon</div>
          </a>
          <a class="menu-item">
            <i class="menu-icon">💸</i>
            Deposit account
            <div class="menu-coming-soon">Coming Soon</div>
          </a>
          <a class="menu-item" href="{{route('home')}}">
            <i class="menu-icon">🏠</i>
            Main Menu
            <div class="menu-arrow"></div>
          </a>
        </div>
        <button class="button-back" onclick="window.history.back()">Back 🔙</button>
      </div>
    </div>
  </section>

@endsection
