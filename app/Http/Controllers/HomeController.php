<?php


namespace App\Http\Controllers;

use App\Http\AMOCrm\AmoCrmActions;
use App\Http\Api\KrakenAPI;
use App\Mail\CloseCheckoutMail;
use App\Models\AmoCrm;
use App\Models\CPaymentCallbacks;
use App\Models\LogActivity;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;


class HomeController
{

    public function index(Request $request)
    {
        $ref = $request->input('ref', null);

        if($ref)
        {
            $request->session()->put('ref', $ref);
        }

        $logActivity =  LogActivity::where('session_id',$request->session()->getId())->first();

        if($logActivity) {
            return response()->redirectToRoute($logActivity->last_step);
        }

        return view('home');

    }

    public function getAccount()
    {
        return view('pages.get_account');
    }

    public function bankAccount(Request $request)
    {

        $data = $request->session()->get('data', null);
        $items = Product::all();

        $items = $items->map(function ($item){
           $item->img =  $item->icon->url();

           return $item;
        });

        return view('pages.bank_account', ['data' => $data, 'items' => $items]);
    }

    public function checkoutView(Request $request)
    {

        $data = [];

        if ($request->session()->has('data')) {
            $data = $request->session()->get('data');
        }

        return view('pages.proceed_checkout', ['items' => $data]);
    }

    public function checkoutFinishView(Request $request)
    {

        $data = $request->session()->get('data', []);
        $code = $request->input('code', null);

        if($code) {
            $cPaymentCallback = CPaymentCallbacks::where('address', $code)->first();

            if ($cPaymentCallback) {
                $data['email'] = $cPaymentCallback->email;
                $data['wallet_id'] = $cPaymentCallback->address;
                $data['sum'] = $cPaymentCallback->amount;
                $data['currency'] = $cPaymentCallback->currency;
                $data['items'] = json_decode($cPaymentCallback->items);
            }

        }

        return view('pages.finish_checkout', ['items' => $data]);
    }

    public function proceedCheckout(Request $request)
    {

        if ($request->input('items', null)) {
            $request->session()->put('data', $request->all());
        }

        return response()->json(['message' => '', 'url' => route('bank.checkout')]);
    }

    public function checkoutFinish(Request $request)
    {
        try {

            $data          = $request->session()->get('data');
            $data['email'] = $request->input('email');
            $referralCode = $request->session()->get('ref', null);

            $sumPrice = 0;
            $itemsAmo = [];

            $amoActions = new AmoCrmActions();

            $strProducts = '';

            foreach ($data['items'] as $item) {
                $sumPrice   += $item['price'];
                $strProducts .= $item['name'] . ', ';
            }

            $itemsAmo[] = [
              'id'    => 160101,
              'value' => $strProducts,
            ];


            $data['sum'] = round($sumPrice / KrakenAPI::getCurrencyRate(), 5);

            $leadId = null;

            if(config('app.debug')) {
                $leadId = 123456;
            } else {
                $leadId          = $amoActions->addLead($data['email'], $sumPrice, $itemsAmo);
            }

            $data['lead_id'] = $leadId;

            $req['currency'] = 'BTC';
            $req['ipn_url'] = config('app.url') . '/api/update-status';
            $req['label'] = $leadId;
            $req['dest_tag'] = $data['email'];

            $response = CoinpaymentController::coinpayments_api_call('get_callback_address', $req);
            $data['wallet_id'] = $response['result']['address'];

            $data['currency'] = 'BTC';

            $cPaymentCallback = CPaymentCallbacks::where('email', $data['email'])->orderBy('updated_at', 'DESC')->first();

            if(!$cPaymentCallback) {
                $cPaymentCallback = new CPaymentCallbacks();
            }

            $cPaymentCallback->email = $data['email'];
            $cPaymentCallback->address = $data['wallet_id'];
            $cPaymentCallback->currency = $data['currency'];
            $cPaymentCallback->lead_id = $leadId;
            $cPaymentCallback->amount = $data['sum'];
            $cPaymentCallback->ipn_url = $req['ipn_url'];
            $cPaymentCallback->items = json_encode($data['items']);
            $cPaymentCallback->real_price = $sumPrice;
            $cPaymentCallback->referral_code = $referralCode;

            $cPaymentCallback->save();


            $request->session()->put('data', $data);


            return response()->json(['error' => false, 'message' => '', 'url' => route('bank.checkout.finish')]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage(), 'url' => null]);
        }

    }

    public function checkoutFinishClose(Request  $request) {
        Log::debug(json_encode($request->all()));

        $link = route('bank.checkout.finish', ['code' => $request->address]);

        Mail::to($request->email)->send(new CloseCheckoutMail($link));

    }



    public function test()
    {
        $amo = new AMOCrmController();
        $link           = config('amo.url') . '/api/v4/leads?filter[pipeline_id]=3676741';
        $amoCrm         = AmoCrm::first();
        $pipelineId     = 3676741;
        $pipelineStatus = null;

        $leads['request']['leads']['list'] = [
          [
            'pipeline_id'          => $pipelineId,
          ],
        ];

        $response = $amo->getRequestBearer($amoCrm->access_token, $link, '', 'GET');

        return $response;

    }


}
